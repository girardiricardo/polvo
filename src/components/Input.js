import React, { Component } from 'react';

export default class Input extends Component {
    render() {
        var v = this.props.barValidations;
        return(
            <div className="input-box">
                <label>{this.props.header}</label>
                <input 
                    type={this.props.type} 
                    className={v === 1 ? "orange" : v === 2 ? "yellow" : v === 3 ? "green" : ""} 
                    value={this.props.value} 
                    onChange={e => this.props.change(e.target.value)} />
            </div>
        );
    }
}
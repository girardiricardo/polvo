import React, { Component } from 'react';
import logo from '../img/logo.svg';

export default class Header extends Component {
    render() {
        return(
            <div className="header-box">
                <img src={logo} alt="Polvo Logo"/>
                <label>Seja bem vindo</label>
            </div>
        );
    }
}
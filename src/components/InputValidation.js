import React, { Component } from 'react';

export default class InputValidation extends Component {
    render() {
        var v = this.props.barValidations;
        return (
            <div className="validation-box">
                <div className="bars">
                    <div className={v === 1 ? "bar orange" : v === 2 ? "bar yellow" : v === 3 ? "bar green" : "bar grey"}/>
                    <div className={v === 2 ? "bar yellow" : v === 3 ? "bar green" : "bar grey"} />
                    <div className={v === 3 ? "bar finalBar green" : "bar finalBar grey"} />
                </div>
                <ul>
                    <li>
                        <span className={this.props.passwordLength ? this.props.passwordLength : "bullet"}></span>
                        <span className="text">Pelo menos 6 caracteres</span>
                    </li>
                    <li>
                        <span className={this.props.passwordCapitals ? this.props.passwordCapitals : "bullet"}></span>
                        <span className="text">Pelo menos 1 letra maiúscula</span>
                    </li>
                    <li>
                        <span className={this.props.passwordNumbers ? this.props.passwordNumbers : "bullet"}></span>
                        <span className="text">Pelo menos 1 número</span>
                    </li>
                </ul>
            </div>
        );
    }
}

import React, { Component } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export default class Bottom extends Component {
    render() {
        var s = this.props.buttonStatus;
        return(
            <div className={"bottom-box"} onClick={this.props.click}>
                <button 
                    className={s === 1 ? "success" : s === 2 ? "warning" : s === 3 ? "error" : "default"}>
                    <span>
                        {s === 1 ? 
                            <FontAwesomeIcon icon="check"/> : 
                        s === 2 ? 
                            <FontAwesomeIcon icon="exclamation"/> :
                        s === 3 ?
                            <FontAwesomeIcon icon="times" /> :
                        "Cadastrar"
                        }
                    </span>
                </button>
            </div>
        );
    }
}
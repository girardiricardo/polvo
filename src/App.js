import React, { Component } from 'react';
import Header from './components/Header';
import Input from './components/Input';
import InputValidation from './components/InputValidation';
import Bottom from './components/Bottom';
import './styles/style.css';

// Icons
import { library } from '@fortawesome/fontawesome-svg-core'
import { faCheck, faExclamation, faTimes } from '@fortawesome/free-solid-svg-icons'
library.add(faCheck);
library.add(faExclamation);
library.add(faTimes);

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      confirmedPassword: '',
      passwordLength: '',
      passwordCapitals: '',
      passwordNumbers: '',
      len: false,
      capitals: false,
      numbers: false,
      barValidations: 0,
      buttonStatus: 0
    };

    this.onChangeEmail = this.onChangeEmail.bind(this);
    this.onChangePassword = this.onChangePassword.bind(this);
    this.onChangeConfirmedPassword = this.onChangeConfirmedPassword.bind(this);
    this.onButtonPress = this.onButtonPress.bind(this);
  }

  onChangeEmail(email) {
    this.setState({ email: email });
  }

  onChangePassword(password) {
    this.setState({ password: password });

    if (password.length === 0) {
      this.setState({
        passwordLength: '',
        passwordCapitals: '',
        passwordNumbers: '',
        barValidations: 0,
      });
    } else {
      password.length >= 6 ? this.setState({ passwordLength: 'bullet bullet-green', len: true }) : this.setState({ passwordLength: 'bullet bullet-orange', len: false });

      (/[ABCDEFGHIJKLMNOPQRSTUVXWYZ]{1}/).exec(password) != null ? this.setState({ passwordCapitals: 'bullet bullet-green', capitals: true }) : this.setState({ passwordCapitals: 'bullet bullet-orange', capitals: false });

      (/[\d]{1}/).exec(password) != null ? this.setState({ passwordNumbers: 'bullet bullet-green', numbers: true }) : this.setState({ passwordNumbers: 'bullet bullet-orange', numbers: false });

      var validate = this.state.len * 1 + this.state.capitals * 1 + this.state.numbers * 1;

      this.setState({ barValidations: validate });
    }
  }

  onChangeConfirmedPassword(confirmedPassword) {
    this.setState({ confirmedPassword: confirmedPassword })
  }

  onButtonPress() {
    if (this.state.email !== '' & this.state.password === this.state.confirmedPassword) {
      this.setState({ buttonStatus: 1 });
    } else if (this.state.email === '' || this.state.password === '' || this.state.confirmedPassword === '') {
      this.setState({ buttonStatus: 2 });
    } else if (this.state.password !== this.state.confirmedPassword) {
      this.setState({ buttonStatus: 3 });
    } else {
      this.setState({ buttonStatus: 0 });
    }
  }

  render() {
    return (
      <div className="login-box">
        <Header />

        <Input header="E-mail" type="text" val={this.state.email} change={this.onChangeEmail} />

        <Input header="Senha" type="password" val={this.state.password} change={this.onChangePassword} barValidations={this.state.barValidations} />

        <InputValidation passwordLength={this.state.passwordLength} passwordCapitals={this.state.passwordCapitals} passwordNumbers={this.state.passwordNumbers} barValidations={this.state.barValidations} />

        <Input header="Confirme sua senha" type="password" val={this.state.confirmedPassword} change={this.onChangeConfirmedPassword} />

        <Bottom click={this.onButtonPress} buttonStatus={this.state.buttonStatus}/>
      </div>
    );
  }
}

export default App;
